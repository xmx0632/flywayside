## 操作说明
0.准备好测试用的数据库服务器,建好对应的数据库db0,db1

1.生成 sql 文件

在 src/main/resources/sql/template/ 目录下，创建 sql 文件模版 V3__Alter_person_table.sql

执行命令

    mvn flyway-side:generatesql

控制台显示日志如下

    [INFO] V1__Create_person_table.sql content:
    create table ### (
    ID int not null,
    NAME varchar(100) not null
    );
    
    [INFO] V1__Create_person_table.sql generated in /flywayside/flywayside-maven-plugin-test/target/sql.
    [INFO] V2__Alter_person_table.sql content:
    ALTER table ### add COLUMN DESC varchar(10) not null;

    
在 /target/sql 目录下找到生成的 V3__Alter_person_table.sql 文件，
复制后到 /flywayside/flywayside-maven-plugin-test/src/main/resources/db/migration_single 目录下

2.执行数据库变更

    mvn flyway-side:migrate
