package org.xmx0632.flywayside.utils;

import junit.framework.TestCase;

/**
 * 
 * @author xmx0632
 *
 */
public class HelperTest extends TestCase {

	public void testFormatPaddingIndex() {
		String index = Helper.formatPaddingIndex(0, 3);
		assertEquals("000", index);
		
		index = Helper.formatPaddingIndex(0, 2);
		assertEquals("00", index);
	}

}
