package org.xmx0632.flywayside.utils;

import junit.framework.TestCase;

/**
 * 
 * @author xmx0632
 *
 */
public class GlobalTableReplaceHolderMapTest extends TestCase {

	public void testGetMap() {
		assertEquals("1_3", GlobalTableReplaceHolderMap.getKey(1, 3));
	}

}
