package org.xmx0632.flywayside;

import java.util.ArrayList;
import java.util.List;

import org.xmx0632.flywayside.param.FlywayParam;

import junit.framework.TestCase;
/**
 * 
 * @author xmx0632
 *
 */
public class BaseFlywaySideMojoTest extends TestCase {

	public void testExecuteFlywayCommand() {

		BaseFlywaySideMojo b = new TestBaseFlywaySideMojo();
		List<FlywayParam> flywayParams = new ArrayList<FlywayParam>();
		flywayParams.add(new FlywayParam(new String[] {"filesystem:src/test/resources/db/migration"}, "testdb", "jdbc:h2:file:./target/demo", "test", "", null,
				FlywayParam.COMMAND_INFO));
		b.executeFlywayCommand(flywayParams);
	}

}
