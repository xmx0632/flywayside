package org.xmx0632.flywayside;

import java.util.List;
import java.util.Map;

import org.apache.maven.plugin.MojoExecutionException;
import org.xmx0632.flywayside.constant.ModelEnum;
import org.xmx0632.flywayside.utils.GlobalTableReplaceHolderMap;

import com.google.inject.internal.util.Lists;

import junit.framework.TestCase;

/**
 * 
 * @author xmx0632
 *
 */
public class BaseFlywaySideMojoModel3Test extends TestCase {

	public void testInitTableMappingModelWithRangeVerifyDetailOk() throws MojoExecutionException {
		BaseFlywaySideMojo b = new TestBaseFlywaySideMojo();
		List<String> tableMappings = Lists.newArrayList("tableName_|tbl_test_|0-15");

		b.setInstanceNumber(2);
		b.setDbNumber(2);
		b.setTableNumberPerDb(2);
		b.setTableMappings(tableMappings);
		b.setModelEnum(ModelEnum.MIX1);

		Map<String, Map<String, String>> map = b.initTableMapping();
		assertEquals(map.size(), 4);

		Map<String, String> tableMap00 = GlobalTableReplaceHolderMap.Instance.getMap()
				.get(GlobalTableReplaceHolderMap.getKey(0, 0));

		assertNotNull(tableMap00);
		assertEquals("tbl_test_000", tableMap00.get("tableName_0"));
		assertEquals("tbl_test_001", tableMap00.get("tableName_1"));

		Map<String, String> tableMap01 = GlobalTableReplaceHolderMap.Instance.getMap()
				.get(GlobalTableReplaceHolderMap.getKey(0, 1));

		assertEquals("tbl_test_000", tableMap01.get("tableName_0"));
		assertEquals("tbl_test_001", tableMap01.get("tableName_1"));

		Map<String, String> tableMap10 = GlobalTableReplaceHolderMap.Instance.getMap()
				.get(GlobalTableReplaceHolderMap.getKey(1, 2));

		assertEquals("tbl_test_000", tableMap10.get("tableName_0"));
		assertEquals("tbl_test_001", tableMap10.get("tableName_1"));
		
		Map<String, String> tableMap11 = GlobalTableReplaceHolderMap.Instance.getMap()
				.get(GlobalTableReplaceHolderMap.getKey(1, 3));

		assertEquals("tbl_test_000", tableMap11.get("tableName_0"));
		assertEquals("tbl_test_001", tableMap11.get("tableName_1"));
	}

	public void testInitTableMappingModelWithRange() throws MojoExecutionException {
		BaseFlywaySideMojo b = new TestBaseFlywaySideMojo();
		List<String> tableMappings = Lists.newArrayList("tableName_1_|tbl_test_|0-15");

		b.setInstanceNumber(2);
		b.setDbNumber(2);
		b.setTableNumberPerDb(4);
		b.setTableMappings(tableMappings);
		b.setModelEnum(ModelEnum.MIX1);

		Map<String, Map<String, String>> map = b.initTableMapping();
		assertEquals(map.size(), 4);
		
	}

	public void testInitTableMappingModelWithEmptyRange() throws MojoExecutionException {
		BaseFlywaySideMojo b = new TestBaseFlywaySideMojo();
		List<String> tableMappings = Lists.newArrayList("tableName_1_|tbl_test_|");

		b.setInstanceNumber(2);
		b.setDbNumber(2);
		b.setTableNumberPerDb(4);
		b.setAllTableNumber(16);
		b.setTableMappings(tableMappings);
		b.setModelEnum(ModelEnum.MIX1);

		Map<String, Map<String, String>> map = b.initTableMapping();
		assertEquals(map.size(), 4);
	}

	public void testInitTableMappingModelWithNoRange() throws MojoExecutionException {
		BaseFlywaySideMojo b = new TestBaseFlywaySideMojo();
		List<String> tableMappings = Lists.newArrayList("tableName_1_|tbl_test_");

		b.setInstanceNumber(2);
		b.setDbNumber(2);
		b.setTableNumberPerDb(4);
		b.setAllTableNumber(16);
		b.setTableMappings(tableMappings);
		b.setModelEnum(ModelEnum.MIX1);

		Map<String, Map<String, String>> map = b.initTableMapping();
		assertEquals(map.size(), 4);
	}
}
