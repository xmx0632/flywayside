package org.xmx0632.flywayside.param;

/**
 * 
 * @author xmx0632
 *
 */
public class IndexParam {

    private int tableIndex ;
    private int dbIndex ;
    private int instIndex ;

    public IndexParam(int instIndex, int dbIndex, int tableIndex) {
        this.instIndex = instIndex;
        this.dbIndex = dbIndex;
        this.tableIndex = tableIndex;
    }


    public int getTableIndex() {
        return tableIndex;
    }

    public int getDbIndex() {
        return dbIndex;
    }

    public int getInstIndex() {
        return instIndex;
    }
}
