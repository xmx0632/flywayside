package org.xmx0632.flywayside.param;

import java.util.Map;

/**
 * 
 * @author xmx0632
 *
 */
public class FlywayParam {

	public static final String COMMAND_MIGRATE = "migrate";
	public static final String COMMAND_INFO = "info";
	public static final String COMMAND_VALIDATE = "validate";
	public static final String COMMAND_REPAIR = "repair";
	public static final String COMMAND_BASELINE = "baseline";

	private String[] locations;
	private String dbName;
	private String url;
	private String user;
	private String password;
	private Map<String, String> placeHoldersMap;

	private String command;

	public FlywayParam(String[] locations, String dbName, String url, String user, String password,
			Map<String, String> placeHoldersMap, String command) {
		this.locations = locations;
		this.dbName = dbName;
		this.url = url;
		this.user = user;
		this.password = password;
		this.placeHoldersMap = placeHoldersMap;
		this.command = command;
	}

	public String getDbName() {
		return dbName;
	}

	public String[] getLocations() {
		return locations;
	}

	public String getUrl() {
		return url;
	}

	public String getUser() {
		return user;
	}

	public String getPassword() {
		return password;
	}

	public Map<String, String> getPlaceHoldersMap() {
		return placeHoldersMap;
	}

	public String getCommand() {
		return command;
	}

	@Override
	public String toString() {
		return " [instance=" + url + "],[dbname=" + dbName + "],[user=" + user + "]";
	}

}
