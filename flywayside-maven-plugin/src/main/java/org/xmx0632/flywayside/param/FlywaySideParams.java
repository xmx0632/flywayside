package org.xmx0632.flywayside.param;

import java.util.List;

/**
 * 
 * @author xmx0632
 *
 */
public class FlywaySideParams {
	private final Integer allTableNumber;

	private String location;

	private String instancePrefix;
	private Integer instanceNumber;
	private List<String> instanceList;

	private String dbPrefix;
	private Integer dbNumber;
	private List<String> dbList;

	private Integer tableNumberPerDb;

	private Integer tablePaddingNumber;

	private List<String> tableMappings;

	public FlywaySideParams(Integer allTableNumber, Integer instanceNumber, List<String> instanceList, String dbPrefix,
			Integer dbNumber, List<String> dbList, Integer tableNumberPerDb, String location, Integer tablePaddingNumber,
			List<String> tableMappings) {
		this.instanceNumber = instanceNumber;
		this.dbNumber = dbNumber;
		this.tableNumberPerDb = tableNumberPerDb;
		this.allTableNumber = allTableNumber;
		this.dbPrefix = dbPrefix;
		this.instanceList = instanceList;
		this.dbList = dbList;
		this.location = location;
		this.tablePaddingNumber = tablePaddingNumber;
		this.tableMappings = tableMappings;
	}

	public Integer getInstanceNumber() {
		return instanceNumber;
	}

	public Integer getDbNumber() {
		return dbNumber;
	}

	public Integer getTableNumberPerDb() {
		return tableNumberPerDb;
	}

	public Integer getAllTableNumber() {
		return allTableNumber;
	}

	public String getInstancePrefix() {
		return instancePrefix;
	}

	public String getDbPrefix() {
		return dbPrefix;
	}

	public List<String> getInstanceList() {
		return instanceList;
	}

	public List<String> getDbList() {
		return dbList;
	}

	public String getLocation() {
		return location;
	}

	public Integer getTablePaddingNumber() {
		return tablePaddingNumber;
	}

	public List<String> getTableMappings() {
		return tableMappings;
	}

	@Override
	public String toString() {
		return super.toString();
	}

}
