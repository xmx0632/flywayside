package org.xmx0632.flywayside.configuration;

import org.xmx0632.flywayside.param.FlywaySideParams;

/**
 * Created by xmx0632 on 2017/10/22.
 */
public interface ConfigurationProvider {

    /**
     * 根据指定文件路径解析出BaseFlywaySideMojo在pom文件中读取的参数对象
     *
     * @param filePath
     * @return TODO 修改为直接从pom文件读取的参数对象（pomParam未实现）
     */
    Object loadConfiguration(String filePath);
}
