package org.xmx0632.flywayside.constant;

/**
 * 
 * @author xmx0632
 *
 */
public class Constant {

    public static final String SPLITTER = "------------------------------------------------------------------------\n";

    public static final String MODEL_1_FOMULAR = "TABLE_INDEX = i % (INST_NUM * DB_NUM * TABLE_NUM)\n" +
            "DB_INDEX = TABLE_INDEX / TABLE_NUM\n" +
            "INST_INDEX = DB_INDEX / DB_NUM\n";

    public static final String MODEL_ONE = "\nexample:\n\ninst0\n" +
            "--db0\n" +
            "----table0\n" +
            "----table1\n" +
            "----table2\n" +
            "----table3\n" +
            "--db1\n" +
            "----table4\n" +
            "----table5\n" +
            "----table6\n" +
            "----table7\n" +
            "inst1\n" +
            "--db2\n" +
            "----table8\n" +
            "----table9\n" +
            "----table10\n" +
            "----table11\n" +
            "--db3\n" +
            "----table12\n" +
            "----table13\n" +
            "----table14\n" +
            "----table15\n"+ SPLITTER;

    public static final String MODEL_2_FOMULAR = "TABLE_INDEX = i % TABLE_NUM\n" +
            "DB_INDEX =  i / TABLE_NUM % DB_NUM\n" +
            "INST_INDEX = i / TABLE_NUM / DB_NUM % INST_NUM\n";

    public static final String  MODEL_TWO = "\nexample:\n\ninst0\n" +
            "--db0\n" +
            "----table0\n" +
            "----table1\n" +
            "----table2\n" +
            "----table3\n" +
            "--db1\n" +
            "----table0\n" +
            "----table1\n" +
            "----table2\n" +
            "----table3\n" +
            "inst1\n" +
            "--db0\n" +
            "----table0\n" +
            "----table1\n" +
            "----table2\n" +
            "----table3\n" +
            "--db1\n" +
            "----table0\n" +
            "----table1\n" +
            "----table2\n" +
            "----table3\n"+ SPLITTER;


    public static final String MODEL_3_FOMULAR = "TABLE_INDEX = i % TABLE_NUM\n" +
            "DB_INDEX =  i / TABLE_NUM % DB_NUM\n" +
            "INST_INDEX = DB_INDEX /  INST_NUM\n";
    
    /**
     * 例子
     */
    public static final String  MODEL_3 = "\nexample:\n\ninst0\n" +
            "--db0\n" +
            "----table0\n" +
            "----table1\n" +
            "----table2\n" +
            "----table3\n" +
            "--db1\n" +
            "----table0\n" +
            "----table1\n" +
            "----table2\n" +
            "----table3\n" +
            "inst1\n" +
            "--db2\n" +
            "----table0\n" +
            "----table1\n" +
            "----table2\n" +
            "----table3\n" +
            "--db3\n" +
            "----table0\n" +
            "----table1\n" +
            "----table2\n" +
            "----table3\n"+ SPLITTER;

    public static final String MODEL_4_FOMULAR = "TABLE_INDEX = i % (INST_NUM * DB_NUM * TABLE_NUM)\n" +
            "DB_INDEX =  TABLE_INDEX / TABLE_NUM % DB_NUM\n" +
            "INST_INDEX =i/TABLE_NUM/ DB_NUM % INST_NUM\n";
    
    public static final String  MODEL_4 = "\nexample:\n\ninst0\n" +
            "--db0\n" +
            "----table0\n" +
            "----table1\n" +
            "----table2\n" +
            "----table3\n" +
            "--db1\n" +
            "----table4\n" +
            "----table5\n" +
            "----table6\n" +
            "----table7\n" +
            "inst1\n" +
            "--db0\n" +
            "----table8\n" +
            "----table9\n" +
            "----table10\n" +
            "----table11\n" +
            "--db1\n" +
            "----table12\n" +
            "----table13\n" +
            "----table14\n" +
            "----table15\n"+ SPLITTER;

}
