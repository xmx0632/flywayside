package org.xmx0632.flywayside.constant;

/**
 * 
 * @author xmx0632
 * Created by xmx0632 on 2017/10/6.
 */
public enum ModelEnum {
	
	/*模式1*/
	MODEL1,
    
	/*模式2*/
    MODEL2,
    
    /*模式3*/
    MIX1,
    
    /*模式4*/
    MIX2
}
