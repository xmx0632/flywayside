package org.xmx0632.flywayside;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.apache.maven.plugins.annotations.Mojo;
import org.xmx0632.flywayside.constant.ModelEnum;
import org.xmx0632.flywayside.handler.BaseHandler;
import org.xmx0632.flywayside.handler.model1.Model1ValidateHandler;
import org.xmx0632.flywayside.handler.model2.Model2ValidateHandler;
import org.xmx0632.flywayside.handler.model3.Model3ValidateHandler;
import org.xmx0632.flywayside.handler.model4.Model4ValidateHandler;
import org.xmx0632.flywayside.param.FlywaySideParams;

/**
 * @author xmx0632
 * 
 * mvn -f pom_model1.xml flyway-side:validate
 */
@Mojo(name = "validate")
public class FlywaySideValidateMojo
        extends BaseFlywaySideMojo {

	@Override
    protected BaseHandler getHandler(FlywaySideParams flywaySideParams) {
        if (ModelEnum.MODEL1 == modelEnum) {
            return new Model1ValidateHandler(flywaySideParams);
        }
        if (ModelEnum.MODEL2 == modelEnum) {
            return new Model2ValidateHandler(flywaySideParams);
        }
        if (ModelEnum.MIX1 == modelEnum) {
            return new Model3ValidateHandler(flywaySideParams);
        }
        if (ModelEnum.MIX2 == modelEnum) {
            return new Model4ValidateHandler(flywaySideParams);
        }

        return null;
    }
}
