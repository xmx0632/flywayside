package org.xmx0632.flywayside.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author xmx0632
 *
 */
public enum GlobalTableReplaceHolderMap {
	
	/**
	 * 单例对象
	 */
	Instance;

	/**
	 *  <dbIndex,<tableReplaceHolder,tableReplacement>>
	 */
	private Map<String, Map<String,String>> map = new HashMap<String, Map<String,String>>();

	public Map<String, Map<String,String>> getMap() {
		return map;
	}
	
	public static String getKey(int instIndex, int dbIndex) {
		return instIndex +"_" +dbIndex;
	}

}
