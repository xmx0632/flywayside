package org.xmx0632.flywayside.utils;

/**
 * 
 * @author xmx0632
 *
 */
public class Helper {

	public static String formatPaddingIndex(int tableIndex, Integer tablePaddingNumber) {
		String format = "%0" + tablePaddingNumber + "d";
		return String.format(format, tableIndex);
	}

}
