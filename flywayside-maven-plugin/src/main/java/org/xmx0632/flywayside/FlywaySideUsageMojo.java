package org.xmx0632.flywayside;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.xmx0632.flywayside.constant.Constant;

/**
 * 
 * @author xmx0632
 *
 */
@Mojo(name = "help")
public class FlywaySideUsageMojo
        extends AbstractMojo {

	@Override
    public void execute()
            throws MojoExecutionException {
        getLog().info("\n\nusage:\n" + Constant.SPLITTER);

        printModel1Usage();

        printModel2Usage();

        printModel3Usage();

        printModel4Usage();
    }

    public void printModel1Usage() {
        String usage = "model1\n数据库和表下标累积的策略:\n\n" +
                "所有的数据库和所有的表的下标都在同一个范围空间内，并不重复：\n" + Constant.MODEL_1_FOMULAR + "\n" + Constant.MODEL_ONE;
        getLog().info(usage);
    }

    public void printModel2Usage() {
        String usage = "model 2\n数据库和表下标归零的策略:\n\n" +
                "数据库在实例范围内重复，表在数据库范围内重复：\n" + Constant.MODEL_2_FOMULAR + "\n" + Constant.MODEL_TWO;
        getLog().info(usage);

    }

    public void printModel3Usage() {
        String usage = "model 3:\n混合策略1\n\n" +
                "数据库在实例范围内不重复，表在数据库范围内重复:\n\n" +
                Constant.MODEL_3_FOMULAR + "\n" + Constant.MODEL_3;
        getLog().info(usage);

    }

    public void printModel4Usage() {
        String usage = "model4:\n混合策略2\n" +
                "\n" +
                "数据库在实例范围内重复，表在数据库范围内不重复:\n\n"
                + Constant.MODEL_4_FOMULAR + "\n" + Constant.MODEL_4;
        getLog().info(usage);

    }

}
