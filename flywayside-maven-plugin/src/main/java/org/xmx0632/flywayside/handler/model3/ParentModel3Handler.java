package org.xmx0632.flywayside.handler.model3;

import java.util.ArrayList;
import java.util.List;

import org.apache.maven.plugin.MojoExecutionException;
import org.xmx0632.flywayside.constant.ModelEnum;
import org.xmx0632.flywayside.handler.BaseHandler;
import org.xmx0632.flywayside.param.FlywaySideParams;
import org.xmx0632.flywayside.param.IndexParam;

/**
 * @author xmx0632
 * 
 * Created by xmx0632 on 2017/10/6.
 */
public class ParentModel3Handler extends BaseHandler {

	public ParentModel3Handler(FlywaySideParams flywaySideParams) {
		super(flywaySideParams);
		this.modelEnum = ModelEnum.MIX1;
	}

	@Override
	protected Integer getTotalTableNumber() {
		return flywaySideParams.getDbNumber() * flywaySideParams.getTableNumberPerDb();
	}

	@Override
	protected IndexParam getIndexParam(int totalTableNumber, int i) {
		String message = "== i:" + i + ", tableNumberPerDb:" + flywaySideParams.getTableNumberPerDb() + ", dbNumber:"
				+ flywaySideParams.getDbNumber() + ", instanceNumber:" + flywaySideParams.getInstanceNumber();
		getLog().debug(message);
		int tableIndex = i % flywaySideParams.getTableNumberPerDb();
		int dbIndex = i / flywaySideParams.getTableNumberPerDb() % flywaySideParams.getDbNumber();
		int instIndex = dbIndex / flywaySideParams.getInstanceNumber();
		getLog().debug("==>" + tableIndex + "-" + dbIndex + "-" + instIndex);

		return new IndexParam(instIndex, dbIndex, tableIndex);
	}

	@Override
	protected void printUsage() {
		helper.printModel3Usage();
	}

	@Override
	protected void validateParameters(int totalTableNumber, FlywaySideParams flywaySideParams)
			throws MojoExecutionException {
		// table总数是否正确
		if (totalTableNumber != flywaySideParams.getAllTableNumber()) {
			throw new MojoExecutionException("config error! totalTableNumber(" + totalTableNumber
					+ ") != allTableNumber(" + flywaySideParams.getAllTableNumber() + ")!");
		}

		// instance总数是否正确
		List<String> instanceList = flywaySideParams.getInstanceList();
		if (isNotEmpty(instanceList) && flywaySideParams.getInstanceNumber() != instanceList.size()) {
			throw new MojoExecutionException("config error! instanceNumber(" + flywaySideParams.getInstanceNumber()
					+ ") and instanceList(" + instanceList.size() + ") not match!");
		}

		// dbList总数是否正确
		List<String> dbList = flywaySideParams.getDbList();
		if (isNotEmpty(dbList) && flywaySideParams.getDbNumber() != dbList.size()) {
			throw new MojoExecutionException("config error! dbNumber(" + flywaySideParams.getDbNumber()
					+ ") and dbList(" + dbList.size() + ") not match!");
		}

	}

	@Override
	protected FlywaySideParams fillDbAndTableListIfNotConfiged(FlywaySideParams flywaySideParams) {
		if (isEmpty(flywaySideParams.getDbList())) {
			List<String> dbList = new ArrayList<String>();

			Integer allDbNumber = flywaySideParams.getDbNumber();
			for (int dbIndex = 0; dbIndex < allDbNumber; dbIndex++) {
				String dbName = flywaySideParams.getDbPrefix() + dbIndex;
				getLog().debug(">>> fill db name:" + dbName);
				dbList.add(dbName);
			}
			flywaySideParams.getDbList().addAll(dbList);
		}

		return flywaySideParams;
	}
}
