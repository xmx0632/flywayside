package org.xmx0632.flywayside.handler.model1;

import java.util.ArrayList;
import java.util.List;

import org.apache.maven.plugin.MojoExecutionException;
import org.xmx0632.flywayside.constant.ModelEnum;
import org.xmx0632.flywayside.handler.BaseHandler;
import org.xmx0632.flywayside.param.FlywaySideParams;
import org.xmx0632.flywayside.param.IndexParam;

/**
 * @author xmx0632
 * 
 *         Created by xmx0632 on 2017/10/6.
 */
public class ParentModel1Handler extends BaseHandler {

	public ParentModel1Handler(FlywaySideParams flywaySideParams) {
		super(flywaySideParams);
		this.modelEnum = ModelEnum.MODEL1;
	}

	@Override
	protected Integer getTotalTableNumber() {
		return flywaySideParams.getInstanceNumber() * flywaySideParams.getDbNumber()
				* flywaySideParams.getTableNumberPerDb();
	}

	@Override
	protected IndexParam getIndexParam(int totalTableNumber, int i) {
		int tableIndex = i % totalTableNumber;
		int dbIndex = tableIndex / flywaySideParams.getTableNumberPerDb();
		int instIndex = dbIndex / flywaySideParams.getDbNumber();
		return new IndexParam(instIndex, dbIndex, tableIndex);
	}

	@Override
	protected void printUsage() {
		helper.printModel1Usage();
	}

	@Override
	protected void validateParameters(int totalTableNumber, FlywaySideParams flywaySideParams)
			throws MojoExecutionException {
		// table总数是否正确
		if (totalTableNumber != flywaySideParams.getAllTableNumber()) {
			throw new MojoExecutionException("config error! totalTableNumber(" + totalTableNumber
					+ ") != allTableNumber(" + flywaySideParams.getAllTableNumber() + ")!");
		}

		// instance总数是否正确
		List<String> instanceList = flywaySideParams.getInstanceList();
		if (isNotEmpty(instanceList) && flywaySideParams.getInstanceNumber() != instanceList.size()) {
			throw new MojoExecutionException("config error! instanceNumber(" + flywaySideParams.getInstanceNumber()
					+ ") and instanceList(" + instanceList.size() + ") not match!");
		}

		// dbList总数是否正确
		List<String> dbList = flywaySideParams.getDbList();

		Integer dbNumberPerInstance = flywaySideParams.getDbNumber();
		Integer totalDbNumber = dbNumberPerInstance * flywaySideParams.getInstanceNumber();
		if (isNotEmpty(dbList) && totalDbNumber != dbList.size()) {
			throw new MojoExecutionException(
					"config error! dbNumber(" + totalDbNumber + ") and dbList(" + dbList.size() + ") not match!");
		}

	}

	@Override
	protected FlywaySideParams fillDbAndTableListIfNotConfiged(FlywaySideParams flywaySideParams) {
		if (isEmpty(flywaySideParams.getDbList())) {
			List<String> dbList = new ArrayList<String>();

			Integer allDbNumberPerInstance = flywaySideParams.getDbNumber();
			int dbNumber = allDbNumberPerInstance * flywaySideParams.getInstanceNumber();
			for (int dbIndex = 0; dbIndex < dbNumber; dbIndex++) {
				String dbName = flywaySideParams.getDbPrefix() + dbIndex;
				getLog().debug(">>> fill db name:" + dbName);
				dbList.add(dbName);
			}
			flywaySideParams.getDbList().addAll(dbList);
		}
		return flywaySideParams;
	}
}
