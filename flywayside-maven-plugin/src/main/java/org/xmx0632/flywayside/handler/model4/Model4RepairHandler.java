package org.xmx0632.flywayside.handler.model4;

import org.xmx0632.flywayside.param.FlywayParam;
import org.xmx0632.flywayside.param.FlywaySideParams;

/**
 * @author xmx0632
 * 
 * Created by xmx0632 on 2017/10/6.
 */
public class Model4RepairHandler extends ParentModel4Handler {

    public Model4RepairHandler(FlywaySideParams flywaySideParams) {
        super(flywaySideParams);
        this.command = FlywayParam.COMMAND_REPAIR;
    }

}
