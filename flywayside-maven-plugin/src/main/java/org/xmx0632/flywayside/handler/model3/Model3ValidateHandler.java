package org.xmx0632.flywayside.handler.model3;

import org.xmx0632.flywayside.param.FlywayParam;
import org.xmx0632.flywayside.param.FlywaySideParams;

/**
 * @author xmx0632
 * 
 * Created by xmx0632 on 2017/10/6.
 */
public class Model3ValidateHandler extends ParentModel3Handler {

    public Model3ValidateHandler(FlywaySideParams flywaySideParams) {
        super(flywaySideParams);
        this.command = FlywayParam.COMMAND_VALIDATE;
    }

}
