package org.xmx0632.flywayside.handler.model2;

import org.xmx0632.flywayside.param.FlywayParam;
import org.xmx0632.flywayside.param.FlywaySideParams;

/**
 * 
 * @author xmx0632
 * 
 * Created by xmx0632 on 2017/10/6.
 */
public class Model2MigrateHandler extends ParentModel2Handler {

    public Model2MigrateHandler(FlywaySideParams flywaySideParams) {
        super(flywaySideParams);
        this.command = FlywayParam.COMMAND_MIGRATE;
    }

}
